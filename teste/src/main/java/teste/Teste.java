package teste;



import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;

import org.junit.Assert;

import org.junit.BeforeClass;

import org.junit.Test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.Select;



import io.github.bonigarcia.wdm.WebDriverManager;



@SuppressWarnings("unused")
public class Teste {



   private static String url = "https://testpages.herokuapp.com/styled/basic-html-form-test.html";

   private static WebDriver driver = null;



   @BeforeClass

   public static void preparandoTeste() {

      WebDriverManager.chromiumdriver().setup();

      driver = new ChromeDriver();

      driver.get(url);

   }



   @AfterClass

   public static void finalizandoTeste() {

      

   }



   @Test

   public void fluxoFeliz() {

      WebElement userName = driver.findElement(By.name("username"));
      userName.sendKeys("QA User");

      WebElement password = driver.findElement(By.name("password"));
      password.sendKeys("QA Password");
      
      
      WebElement dropdown = driver.findElement(By.name("dropdown"));

      Select select = new Select(dropdown);
      select.selectByValue("dd5");
      
      WebElement comments = driver.findElement(By.name("comments"));
      comments.clear();
      comments.sendKeys("Comentario");
      
      WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
      
      Select selectMultipleSelect = new Select(multipleSelect);
      selectMultipleSelect.selectByValue("ms4");
      
      WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
      checkbox2.click();
      
      WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
      checkbox1.click();
      
      WebElement radio3 = driver.findElement(By.xpath("//input[@value='rd3']"));
      radio3.click();
      
      WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));
      buttonSubmit.click();
      
      Assert.assertEquals("Testando", driver.findElement(By.id("_valueusername")).getText());
      Assert.assertEquals("teste", driver.findElement(By.id("_valuepassword")).getText());
      Assert.assertEquals("teste", driver.findElement(By.id("_valuecomments")).getText());
      
      
      
      	


   }



}
